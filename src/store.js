import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [],
    cart: []
  },
  getters: {
    categories(state) {
      const { products } = state;

      const categories = state.products
        .filter(product => !product.parent_id)
        .sort((a, b) => a.order - b.order);

      // Add brands for each category
      categories.forEach(category => {
        const brands = products
          .filter(product => {
            if (product.parent_id) {
              const parent = products.find(parent => {
                return parent.id == product.parent_id;
              });

              return parent && category.id == parent.id;
            } else {
              return false;
            }
          })
          .sort((a, b) => a.order - b.order);

        // Add products for each brand
        brands.forEach(brand => {
          brand.products = products
            .filter(product => {
              return product.parent_id == brand.id;
            })
            .sort((a, b) => a.order - b.order);
        });

        category.brands = brands;
      });

      return categories;
    },
    cart(state) {
      return state.cart;
    },
    cartTotal(state) {
      return state.cart.reduce((total, item) => {
        return total + item.final_price * item.quantity;
      }, 0);
    },
    cartTotalQuantity(state) {
      return state.cart.reduce((total, item) => {
        return total + item.quantity;
      }, 0);
    }
  },
  mutations: {
    SET_PRODUCTS(state, products) {
      state.products = products;
    },
    ADD_TO_CART(state, orderedProduct) {
      const existingProduct = state.cart.find(product => {
        return product.id == orderedProduct.id;
      });

      if (!existingProduct) {
        state.cart.push(orderedProduct);
      } else {
        ++existingProduct.quantity;
      }
    },
    CHANGE_ITEM_QUANTITY(state, { item, quantity }) {
      const product = state.cart.find(product => product.id == item.id);
      product.quantity = quantity;
    },
    INCREASE_ITEM_QUANTITY(state, item) {
      const product = state.cart.find(product => product.id == item.id);
      ++product.quantity;
    },
    DECREASE_ITEM_QUANTITY(state, item) {
      const product = state.cart.find(product => product.id == item.id);
      --product.quantity;
    },
    REMOVE_FROM_CART(state, item) {
      state.cart = state.cart.filter(product => product.id != item.id);
    },
    ADD_PRODUCT(state, product) {
      state.products.push(product);
    },
    EDIT_PRODUCT(state, editableProduct) {
      const product = state.products.find(
        product => product.id == editableProduct.id
      );
      for (let key in editableProduct) {
        product[key] = editableProduct[key];
      }
    },
    DELETE_PRODUCT(state, removableProduct) {
      state.products = state.products.filter(
        product => product.id != removableProduct.id
      );
    }
  },
  actions: {
    setProducts({ commit }, products) {
      commit("SET_PRODUCTS", products);
    },
    addToCart({ commit }, product) {
      commit("ADD_TO_CART", {
        ...product,
        final_price: product.on_sale ? product.sale_price : product.price,
        quantity: 1
      });
    },
    changeItemQuantity({ commit }, { item, quantity }) {
      if (+quantity) {
        commit("CHANGE_ITEM_QUANTITY", { item, quantity });
      } else {
        commit("REMOVE_FROM_CART", item);
      }
    },
    increaseQuantity({ commit }, item) {
      commit("INCREASE_ITEM_QUANTITY", item);
    },
    decreaseQuantity({ state, commit }, item) {
      const cartItem = state.cart.find(cartItem => cartItem.id == item.id);

      if (cartItem) {
        if (cartItem.quantity > 1) {
          commit("DECREASE_ITEM_QUANTITY", item);
        } else {
          commit("REMOVE_FROM_CART", item);
        }
      } else {
        return;
      }
    },
    addEntity({ commit }, product) {
      product.id = uuidv4();
      commit("ADD_PRODUCT", product);
    },
    editEntity({ commit }, product) {
      commit("EDIT_PRODUCT", product);
    },
    deleteEntity({ commit }, product) {
      commit("DELETE_PRODUCT", product);
    }
  }
});

/**
 * UUIDv4 generator
 * https://stackoverflow.com/a/2117523/11112270
 */
function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}
